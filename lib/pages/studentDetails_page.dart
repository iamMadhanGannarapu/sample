import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:todo_sample/modals/studentModel.dart';
import 'package:todo_sample/services/student_service.dart';

class StudentDetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<ChartData> chartData = [
      ChartData('Telugu', 72, Color.fromRGBO(9, 0, 136, 1)),
      ChartData('Hinid', 63, Color.fromRGBO(147, 0, 119, 1)),
      ChartData('English', 82, Color.fromRGBO(228, 0, 124, 1)),
      ChartData('Maths', 99, Color.fromRGBO(255, 189, 57, 1)),
      ChartData('Science', 78, Color.fromRGBO(255, 100, 57, 1)),
      ChartData('Social', 60, Color.fromRGBO(255, 189, 122, 1)),
    ];
    return Consumer<StudentService>(builder: (_, model, __) {
      print(model?.student?.marks?.quartely?.telugu ?? '--');
      print(
          '${model?.student?.firstName?.toUpperCase()}  ${model?.student?.lastName?.toUpperCase()}' ??
              '');

      return Scaffold(
        appBar: AppBar(
          title: Text(
            '${model?.student?.firstName?.toUpperCase()}  ${model?.student?.lastName?.toUpperCase()}' ??
                '',
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          leading: IconButton(
            icon:
                Icon(Icons.keyboard_arrow_left, size: 40, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 300,
                    child: Column(
                      children: <Widget>[
                        _labels(
                            subject: 'Telugu',
                            color: Color.fromRGBO(9, 0, 136, 1)),
                        _labels(
                            subject: 'Hindi',
                            color: Color.fromRGBO(147, 0, 119, 1)),
                        _labels(
                            subject: 'English',
                            color: Color.fromRGBO(228, 0, 124, 1)),
                        _labels(
                            subject: 'Maths',
                            color: Color.fromRGBO(255, 189, 57, 1)),
                        _labels(
                            subject: 'Science',
                            color: Color.fromRGBO(255, 100, 57, 1)),
                        _labels(
                            subject: 'Social',
                            color: Color.fromRGBO(255, 189, 122, 1)),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Container(
                    // color: Colors.lightBlue,
                    height: 300,
                    // width: 300,
                    child: SfCircularChart(
                      series: <CircularSeries>[
                        DoughnutSeries<ChartData, String>(
                            dataSource: chartData,
                            pointColorMapper: (ChartData data, _) => data.color,
                            xValueMapper: (ChartData data, _) => data.x,
                            yValueMapper: (ChartData data, _) => data.y)
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container()
          ],
        ),
      );
    });
  }

  Widget _labels({String subject, Color color}) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
            child: Container(
              color: color,
            ),
          ),
          SizedBox(
            height: 20,
            width: 10,
          ),
          Text(
            subject,
          ),
        ],
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);

  final String year;
  final double sales;
}

class ChartData {
  ChartData(this.x, this.y, [this.color]);
  final String x;
  final double y;
  final Color color;
}

class Marks {
  final int telugu;
  final int hindi;
  final int english;
  final int maths;
  final int science;
  final int social;

  Marks(
      {this.telugu,
      this.hindi,
      this.english,
      this.maths,
      this.science,
      this.social});
}
