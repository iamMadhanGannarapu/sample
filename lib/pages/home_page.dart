import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_sample/routes/router_constants.dart';
import 'package:todo_sample/services/addStudent_service.dart';
import 'package:todo_sample/services/login_service.dart';
import 'package:todo_sample/services/students_service.dart';
import 'package:todo_sample/widgets/modal_pop.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  addStd() {
    print('object');
  }

  @override
  Widget build(BuildContext context) {
    print('home page builded');
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Theme.of(context).buttonTheme.colorScheme.secondary,
          ),
          onPressed: () => showDialog(
            context: context,
            child: MyDialog(
              onValueChange: ({firstName, lastName, email, gender}) async {
                print(
                    'function called $firstName,\n $lastName,\n $email,\n $gender');
                await Provider.of<AddStudentService>(context, listen: false)
                    .addStudent(
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        gender: gender);
                await Provider.of<StudentsService>(context, listen: false)
                    .getStudents();
                Navigator.of(context).pop();
              },
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                height: 35,
                margin: EdgeInsets.only(top: 10, left: 10),
                child: Consumer<LoginService>(
                  builder: (_, model, __) {
                    return Container(
                      margin: EdgeInsets.all(5),
                      height: 50,
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '${model?.userDetails?.name?.toUpperCase() ?? '-'}',
                            style: TextStyle(
                                fontSize:
                                    Theme.of(context).textTheme.title.fontSize),
                          ),
                          GestureDetector(
                            onTap: () {
                              print('image tapped');
                              Navigator.pushNamed(context, ProfilePageRoute);
                            },
                            child: CircleAvatar(
                              radius: 20.0,
                              backgroundImage: NetworkImage(
                                  model?.userDetails?.image ?? '-'),
                              backgroundColor: Colors.transparent,
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),
            Expanded(
              flex: 9,
              child: Container(
                height: 650,
                child: Consumer<StudentsService>(
                  builder: (_, studentModel, __) {
                    var students = studentModel.items;
                    return Padding( 
                      padding: EdgeInsets.all(10),
                      child: ListView.builder(
                        itemCount: students.length,
                        itemBuilder: (_, i) => GestureDetector(
                          onTap: () {
                            print(students[i].id);
                            Navigator.pushNamed(
                              context,
                              StudentDetailsSceenRoute,
                              arguments: <String, dynamic>{
                                'studentId': students[i].id.toString(),
                              },
                            );
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                  '${students[i].id.toString()}. ${students[i].firstName} ${students[i].lastName}'),
                              Divider()
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
