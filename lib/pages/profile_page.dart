import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_sample/services/login_service.dart';

class ProfilePage extends StatelessWidget {
  List _buildList(int count) {
    List<Widget> listItems = List();

    for (int i = 0; i < count; i++) {
      listItems.add(Padding(
          padding: EdgeInsets.all(20.0),
          child:
              Text('Item ${i.toString()}', style: TextStyle(fontSize: 25.0))));
    }

    return listItems;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Consumer<LoginService>(
          builder: (_, model, __) {
            return Column(
              children: <Widget>[
                Container(
                  height: 250,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment(0.8, 0.0),
                      colors: [
                        const Color(0xFF3366FF),
                        const Color(0xFF00CCFF),
                      ],
                      tileMode: TileMode.repeated,
                    ),
                  ),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 40, horizontal: 15),
                        height: 150,
                        width: double.infinity,
                        // color: Colors.white,
                        child: Card(
                          elevation: 30,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              ClipRRect(
                                borderRadius: BorderRadius.circular(5.0),
                                child: ClipPath(
                                  clipper: MyCustomClipper(),
                                  child:
                                      Image.network(model?.userDetails?.image),
                                ),
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsetsDirectional.only(top:15),
                                    child: Text(
                                      model?.userDetails?.name?.toUpperCase() ??
                                          '',
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                   Text(
                                    'Vaagdevi high school',
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black54,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    '+91 9963677093',
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black54,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    'v_madhan@ljx.com.au',
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black54,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 200,
                  width: double.infinity,
                  // color: Colors.redAccent,
                  child: Column(
                    children: <Widget>[
                      Text(model?.userDetails?.name ?? ''),
                      Text(model?.userDetails?.name ?? ''),
                      Text(model?.userDetails?.name ?? ''),
                      Text(model?.userDetails?.name ?? ''),
                    ],
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path(); // the starting point is the 0,0 position of the widget.
    // path.lineTo(0, size.height); // this draws the line from current point to the left bottom position of widget
    // path.lineTo(size.width, size.height); // this draws the line from current point to the right bottom position of the widget.
    // path.lineTo(size.width, 0.8); // this draws the line from current point to the right top position of the widget
    // path.close(); // this closes the loop from current position to the starting point of widget

    // Draw a straight line from current point to the bottom left corner.
    path.lineTo(0.0, size.height);
    path.lineTo(75.0, size.height);
    // Draw a straighsize.width line from current point to the top right corner.;
    path.lineTo(size.height, 0.0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return false;
  }
}
