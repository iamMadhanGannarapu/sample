import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_sample/routes/router_constants.dart';
import 'package:todo_sample/services/login_service.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  EdgeInsetsGeometry margin = EdgeInsets.only(left: 30, right: 30);

  _login() async {
    String username = _usernameController.text;
    String password = _passwordController.text;
    bool loginSucceeded =
        await Provider.of<LoginService>(context, listen: false)
            .login(username, password);
    if (loginSucceeded) {
      Navigator.pushNamed(context, HomeScreenRoute);
    }
  }

  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          color: Colors.grey[200],
          margin: EdgeInsets.only(top: 200),
          child: Column(
            children: <Widget>[
              Container(
                child: TabBar(
                  unselectedLabelColor: Colors.grey,
                  labelColor: Theme.of(context).primaryColor,
                  indicatorColor: Theme.of(context).primaryColor,
                  tabs: [
                    Tab(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(Icons.lock_open),
                          Text(
                            'Login',
                            style: TextStyle(fontSize: 20),
                          )
                        ],
                      ),
                    ),
                    Tab(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(
                            Icons.child_care,
                            size: 25,
                          ),
                          Text(
                            'SignUp',
                            style: TextStyle(fontSize: 20),
                          )
                        ],
                      ),
                    ),
                  ],
                  controller: _tabController,
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                height: 400,
                child: TabBarView(
                  children: [
                    _loginTabView(),
                    _signUp(),
                  ],
                  controller: _tabController,
                ),
              ),
            ],
          ),
        ),
      )),
    );
  }

  Widget _signUp() {
    return Container(
      child: Column(
        children: <Widget>[
          _textField(
              labelText: 'username',
              hintText: 'Enter your user name',
              controller: _usernameController, icon: Icons.person_outline,),
          _textField(
              labelText: 'password',
              hintText: 'Enter your password',
              controller: _passwordController, icon: Icons.lock_outline,),
          _textField(
              labelText: 'Confirm password', hintText: 'Confirm password', icon: Icons.lock_outline,),
          _textField(
              labelText: 'Email', hintText: 'Enter your email address', icon: Icons.mail_outline,),
          Spacer(),
          SizedBox(
            child: RaisedButton(child: Text('Signup'), onPressed: _login),
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _loginTabView() {
    return Container(
      child: Column(
        children: <Widget>[
            Spacer(),
          _textField(
            labelText: 'username',
            hintText: 'Enter your user name',
            controller: _usernameController,
            icon: Icons.person_outline,
          ),
          _textField(
              labelText: 'password',
              hintText: 'Enter your password',
              controller: _passwordController,
               icon: Icons.lock_open,),
          Spacer(),
          SizedBox(
            child: RaisedButton(child: Text('Login'), onPressed: _login),
          ),
          Spacer(
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _textField(
      {String labelText,
      String hintText,
      TextEditingController controller,
      IconData icon}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: TextFormField(
        decoration: InputDecoration(
            labelText: labelText,
            hintText: hintText,
            // border: UnderlineInputBorder(),
            filled: true,
            icon: Icon(icon)),
        controller: controller,
      ),
    );
  }
}
