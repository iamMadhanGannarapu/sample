import 'package:flutter/material.dart';

class MyDialog extends StatefulWidget {
  const MyDialog({this.onValueChange, this.initialValue});

  final String initialValue;
  final Function(
      {String firstName,
      String lastName,
      String email,
      String gender}) onValueChange;

  @override
  State createState() => new MyDialogState();
}

class MyDialogState extends State<MyDialog> {
  final _firstNameController = TextEditingController();
  final _lastnameController = TextEditingController();
  final _emailController = TextEditingController();

  String _radioValue;
  String gender;

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'male':
          gender = value;
          break;
        case 'female':
          gender = value;
          break;
        default:
          gender = null;
      }
      // debugPrint(choice); //Debug the choice in console
    });
  }

  // addStudent() async {
  //   print(
  //       'fn:${_firstNameController.text}\nln: ${_lastnameController.text}\ngender:$gender');
  //   await Provider.of<AddStudentService>(context, listen: false).addStudent(
  //       firstName: _firstNameController.text,
  //       lastName: _lastnameController.text,
  //       email: _emailController.text,
  //       gender: gender);
  //   Navigator.of(context).pop();
  // }

  Widget build(BuildContext context) {
    return new SimpleDialog(
      title: Text(
        'Enter Student Details',
        style: TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.w500,
          // color: const Color(0xFF1BC0C5),
        ),
      ),
      children: <Widget>[
        new Container(
          padding: const EdgeInsets.all(10.0),
          // margin: const EdgeInsets.all(0.0),
          child: Container(
            // color: Colors.red,
            margin: EdgeInsets.all(0),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                        labelText: "First Name",
                        hintText: 'Enter your first name'),
                    controller: _firstNameController,
                  ),
                  TextField(
                    decoration: InputDecoration(
                        labelText: "Last Name",
                        hintText: ' Enter your last name'),
                    controller: _lastnameController,
                  ),
                  TextField(
                    decoration: InputDecoration(
                        labelText: "Email", hintText: ' Enter your email'),
                    controller: _emailController,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        'Gender:',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          // color: const Color(0xFF1BC0C5),
                        ),
                      ),
                      Radio(
                          value: 'male',
                          groupValue: _radioValue,
                          onChanged: radioButtonChanges),
                      Text(
                        'male',
                        style: TextStyle(fontSize: 16.0),
                      ),
                      Radio(
                          value: 'female',
                          groupValue: _radioValue,
                          onChanged: radioButtonChanges),
                      Text(
                        'Female',
                        style: TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    width: 320.0,
                    child: RaisedButton(
                      onPressed:()=> widget.onValueChange(
                          firstName: _firstNameController.text,
                          lastName: _lastnameController.text,
                          email: _emailController.text,
                          gender: gender),
                      child: Text(
                        "Save",
                        // style: TextStyle(color: Colors.white),
                      ),
                      // color: const Color(0xFF1BC0C5),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
