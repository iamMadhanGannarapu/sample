import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AddStudentService with ChangeNotifier {
  Future<void> addStudent(
      {String firstName, String lastName, String email, String gender}) async {
    final url = 'http://10.0.2.2:3000/students';
    try {
      final response = await http.post(
        url,
        body: {
          "firstName": firstName,
          "lastName": lastName,
          "email": email,
          "gender": gender
        },
      );
      print(response);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }
}
