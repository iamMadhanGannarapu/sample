import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:todo_sample/modals/studentModel.dart';
import 'package:todo_sample/modals/student_model.dart';
import 'package:todo_sample/models/studentDetails.dart';

class StudentService with ChangeNotifier {

  StdModel _student;

  StdModel get student {
    return _student;
  }

  Future<void> getStudent(String studentId) async {
    print('get studnet called');
    final String url = 'http://10.0.2.2:3000/students?id=$studentId';

    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body);
      if (extractedData == null) {
        return;
      }
      // print(extractedData[0]['marks']);
      // _student= extractedData[0] as StudentDetails;
    _student =  StdModel.fromJson(extractedData[0]);
      // _student = StdModel(extractedData[0]
      //   // extractedData[0]
      //   // id: extractedData[0]['id'],
      //   // email: extractedData[0]['email'],
      //   // firstName: extractedData[0]['firstName'],
      //   // gender: extractedData[0]['gender'],
      //   // lastName: extractedData[0]['lastName'],
      //   // marks: extractedData[0]['marks'],
      // );
      notifyListeners();
    } catch (e) {
      throw (e);
    }
  }
}
