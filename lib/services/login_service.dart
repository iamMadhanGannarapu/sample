import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class UserDetails {
  String password;
  String name;
  int mobileNo;
  int userId;
  String image;
}

class LoginService with ChangeNotifier {
  // print('');
  UserDetails userDetails = UserDetails();

  Future<bool> login(String username, String password) async {
    print('object');

    bool loginSucceeded = true;
    username = 'madhan gannarapu';
    password = 'nani';

    try {
      final response = await http
          .get('http://10.0.2.2:3000/users?name=$username&password=$password');
      final extractedData = json.decode(response.body);
      if (response.statusCode == 200 && extractedData[0]['name'].isNotEmpty) {
        print('logged-in successfully!');
        userDetails.name = extractedData[0]['name'];
        userDetails.mobileNo = extractedData[0]['mobileNo'];
        userDetails.password = extractedData[0]['password'];
        userDetails.userId = extractedData[0]['id'];
        userDetails.image = extractedData[0]['image'];
        notifyListeners();
      } else {
        loginSucceeded = false;
      }
    } catch (e, stacktrace) {
      print('error message: ${e.message} at: \n$stacktrace');
      loginSucceeded = false;
    }
    return loginSucceeded;
  }
}
