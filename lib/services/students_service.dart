import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:todo_sample/modals/student_model.dart';

class StudentsService with ChangeNotifier {
  List<Student> _items = [];
  List<Student> get items {
    return [..._items];
  }

  final String url = 'http://10.0.2.2:3000/students';

  Future<void> getStudents() async {
    print('get studnets called');

    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body);
      if (extractedData == null) {
        return;
      }
      final List<Student> loadedstudents = [];
      extractedData.forEach((student) {
        loadedstudents.add(Student(
          id: student['id'],
          email: student['email'],
          firstName: student['firstName'],
          gender: student['gender'],
          lastName: student['lastName'],
        ));
      });
      _items = loadedstudents;
      notifyListeners();
    } catch (e) {
      throw (e);
    }
  }
}
