import 'package:json_annotation/json_annotation.dart';
import "marks.dart";
part 'studentDetails.g.dart';

@JsonSerializable()
class StudentDetails {
    StudentDetails();

    num id;
    String firstName;
    String lastName;
    String email;
    String gender;
    Marks marks;
    
    factory StudentDetails.fromJson(Map<String,dynamic> json) => _$StudentDetailsFromJson(json);
    Map<String, dynamic> toJson() => _$StudentDetailsToJson(this);
}
