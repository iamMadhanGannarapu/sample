// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quartely.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Quartely _$QuartelyFromJson(Map<String, dynamic> json) {
  return Quartely()
    ..telugu = json['telugu'] as num
    ..hindi = json['hindi'] as num
    ..english = json['english'] as num
    ..maths = json['maths'] as num
    ..science = json['science'] as num
    ..social = json['social'] as num;
}

Map<String, dynamic> _$QuartelyToJson(Quartely instance) => <String, dynamic>{
      'telugu': instance.telugu,
      'hindi': instance.hindi,
      'english': instance.english,
      'maths': instance.maths,
      'science': instance.science,
      'social': instance.social
    };
