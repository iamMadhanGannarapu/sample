import 'package:json_annotation/json_annotation.dart';

part 'halfYearrly.g.dart';

@JsonSerializable()
class HalfYearrly {
    HalfYearrly();

    num telugu;
    num hindi;
    num english;
    num maths;
    num science;
    num social;
    
    factory HalfYearrly.fromJson(Map<String,dynamic> json) => _$HalfYearrlyFromJson(json);
    Map<String, dynamic> toJson() => _$HalfYearrlyToJson(this);
}
