import 'package:json_annotation/json_annotation.dart';

part 'quartely.g.dart';

@JsonSerializable()
class Quartely {
    Quartely();

    num telugu;
    num hindi;
    num english;
    num maths;
    num science;
    num social;
    
    factory Quartely.fromJson(Map<String,dynamic> json) => _$QuartelyFromJson(json);
    Map<String, dynamic> toJson() => _$QuartelyToJson(this);
}
