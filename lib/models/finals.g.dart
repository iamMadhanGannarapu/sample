// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'finals.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Finals _$FinalsFromJson(Map<String, dynamic> json) {
  return Finals()
    ..telugu = json['telugu'] as num
    ..hindi = json['hindi'] as num
    ..english = json['english'] as num
    ..maths = json['maths'] as num
    ..science = json['science'] as num
    ..social = json['social'] as num;
}

Map<String, dynamic> _$FinalsToJson(Finals instance) => <String, dynamic>{
      'telugu': instance.telugu,
      'hindi': instance.hindi,
      'english': instance.english,
      'maths': instance.maths,
      'science': instance.science,
      'social': instance.social
    };
