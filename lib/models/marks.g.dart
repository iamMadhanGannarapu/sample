// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'marks.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Marks _$MarksFromJson(Map<String, dynamic> json) {
  return Marks()
    ..quartely = json['quartely'] == null
        ? null
        : Quartely.fromJson(json['quartely'] as Map<String, dynamic>)
    ..halfYearrly = json['halfYearrly'] == null
        ? null
        : HalfYearrly.fromJson(json['halfYearrly'] as Map<String, dynamic>)
    ..finals = json['finals'] == null
        ? null
        : Finals.fromJson(json['finals'] as Map<String, dynamic>);
}

Map<String, dynamic> _$MarksToJson(Marks instance) => <String, dynamic>{
      'quartely': instance.quartely,
      'halfYearrly': instance.halfYearrly,
      'finals': instance.finals
    };
