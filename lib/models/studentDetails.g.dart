// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'studentDetails.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StudentDetails _$StudentDetailsFromJson(Map<String, dynamic> json) {
  return StudentDetails()
    ..id = json['id'] as num
    ..firstName = json['firstName'] as String
    ..lastName = json['lastName'] as String
    ..email = json['email'] as String
    ..gender = json['gender'] as String
    ..marks = json['marks'] == null
        ? null
        : Marks.fromJson(json['marks'] as Map<String, dynamic>);
}

Map<String, dynamic> _$StudentDetailsToJson(StudentDetails instance) =>
    <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'email': instance.email,
      'gender': instance.gender,
      'marks': instance.marks
    };
