import 'package:json_annotation/json_annotation.dart';

part 'finals.g.dart';

@JsonSerializable()
class Finals {
    Finals();

    num telugu;
    num hindi;
    num english;
    num maths;
    num science;
    num social;
    
    factory Finals.fromJson(Map<String,dynamic> json) => _$FinalsFromJson(json);
    Map<String, dynamic> toJson() => _$FinalsToJson(this);
}
