import 'package:json_annotation/json_annotation.dart';
import "quartely.dart";
import "halfYearrly.dart";
import "finals.dart";
part 'marks.g.dart';

@JsonSerializable()
class Marks {
    Marks();

    Quartely quartely;
    HalfYearrly halfYearrly;
    Finals finals;
    
    factory Marks.fromJson(Map<String,dynamic> json) => _$MarksFromJson(json);
    Map<String, dynamic> toJson() => _$MarksToJson(this);
}
