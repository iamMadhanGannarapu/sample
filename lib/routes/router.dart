// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:todo_providers/providers/login_provider.dart';
// import 'package:todo_providers/routes/router_constants.dart';
// import 'package:todo_providers/screens/Home_screen.dart';
// import 'package:todo_providers/screens/login_screen.dart';

// Route<dynamic> generateRoute(RouteSettings settings) {
//   switch (settings.name) {
//     case LoginScreenRoute:
//       return MaterialPageRoute(builder: (_) {
//         return ChangeNotifierProvider(
//           create: (_) => new LoginProvider(),
//           child: LoginScreen(),
//         );
//       });
//     case HomeScreenRoute:
//       return MaterialPageRoute(builder: (_) {
//         return ChangeNotifierProvider<LoginProvider>.value(
//           value: LoginProvider(),
//           child: HomeScreen(),
//         );
//       });
//     default:
//       return null;
//   }
// }
