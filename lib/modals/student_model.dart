import 'package:flutter/foundation.dart';

class Student {
  final int id;
  final String firstName;
  final String lastName;
  final String email;
  final String gender;
  // final Marks marks;

  //  final String  image;

  Student(
      {@required this.id,
      @required this.email,
      @required this.firstName,
      @required this.lastName,
      @required this.gender,
      // @required this.marks
      // @required this.image
      });

  factory Student.fromJson(Map<String, dynamic> json) {
    return Student(
      id: json['id'] as int,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      email: json['email'] as String,
      gender: json['gender'] as String,
      //  image: json['image'] as String,
    );
  }
}

class Subject {
  final int telugu;
  final int english;
  final int hindi;
  final int maths;
  final int science;
  final int social;
  Subject({
    @required this.telugu,
    @required this.hindi,
    @required this.english,
    @required this.maths,
    @required this.science,
    @required this.social,
  });
  factory Subject.fromJson(Map<String, dynamic> json) {
    return Subject(
      telugu: json['telugu'] as int,
      hindi: json['hindi'] as int,
      english: json['english'] as int,
      maths: json['maths'] as int,
      science: json['science'] as int,
      social: json['social'] as int,
    );
  }
}

class Exam {
  final Subject subject;
  Exam({this.subject});
  factory Exam.fromJson(Map<String, dynamic> json) {
    return Exam(subject: json['exam'] as Subject);
  }
  // profile =
  // json['profile'] != null ? Profile.fromJson(json['profile']) : null;
}

class Marks {
  final Exam exam;
  Marks(this.exam);
}

// class Subject {
//   final String subjectName;
//   final int subjectMarks;
//   Subject({this.subjectMarks, this.subjectName});
// }
