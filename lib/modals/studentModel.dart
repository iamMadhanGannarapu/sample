class Quartely {
  int telugu;
  int hindi;
  int englihs;
  int maths;
  int science;
  int social;

  Quartely(
      {this.telugu,
      this.hindi,
      this.englihs,
      this.maths,
      this.science,
      this.social});
  factory Quartely.fromJson(Map<String, dynamic> parsedJson) {
    return Quartely(
      telugu: parsedJson['telugu'],
      hindi: parsedJson['hindi'],
      englihs: parsedJson['englihs'],
      maths: parsedJson['maths'],
      science: parsedJson['science'],
      social: parsedJson['social'],
    );
  }
}

class HalfYearly {
  int telugu;
  int hindi;
  int englihs;
  int maths;
  int science;
  int social;
  HalfYearly(
      {this.telugu,
      this.hindi,
      this.englihs,
      this.maths,
      this.science,
      this.social});
  factory HalfYearly.fromJson(Map<String, dynamic> parsedJson) {
    return HalfYearly(
      telugu: parsedJson['telugu'],
      hindi: parsedJson['hindi'],
      englihs: parsedJson['englihs'],
      maths: parsedJson['maths'],
      science: parsedJson['science'],
      social: parsedJson['social'],
    );
  }
}

class Marks {
  Quartely quartely;
  HalfYearly halfYearly;
  Marks({this.quartely, this.halfYearly});
  factory Marks.fromJson(Map<String, dynamic> parsedJson) {
    return Marks(
        quartely: Quartely.fromJson(parsedJson['quartely']),
        halfYearly: HalfYearly.fromJson(parsedJson['halfYearly']));
  }
}

class StdModel {
  int id;
  String firstName;
  String lastName;
  String email, gender;
  Marks marks;
  StdModel(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.gender,
      this.marks});

  factory StdModel.fromJson(Map<String, dynamic> parsedJson) {
    return StdModel(
      id: parsedJson['id'],
      firstName: parsedJson['firstName'],
      lastName: parsedJson['lastName'],
      email: parsedJson['email'],
      gender: parsedJson['gender'],
      marks: Marks.fromJson(parsedJson['marks']),
    );
  }
}
