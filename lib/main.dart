import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_sample/pages/home_page.dart';
import 'package:todo_sample/pages/login_page.dart';
import 'package:todo_sample/pages/profile_page.dart';
import 'package:todo_sample/pages/studentDetails_page.dart';
import 'package:todo_sample/routes/router_constants.dart';
import 'package:todo_sample/services/addStudent_service.dart';
import 'package:todo_sample/services/login_service.dart';
import 'package:todo_sample/services/student_service.dart';
import 'package:todo_sample/services/students_service.dart';
import 'package:todo_sample/theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const Color baseColor = Color(0xFF1BC0C5);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginService>(
          create: (context) => LoginService(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        // darkTheme: ThemeData.dark(),
        // theme: myTheme,

        theme: ThemeData(
          primaryColor: baseColor,
          accentColor: baseColor,
          brightness: Brightness.light,
          buttonTheme: ButtonThemeData(
            buttonColor: baseColor,
            textTheme: ButtonTextTheme.accent,
            colorScheme:
                Theme.of(context).colorScheme.copyWith(secondary: Colors.white),
          ),
          textTheme: TextTheme(
            headline: TextStyle(fontSize: 36),
            title: TextStyle(fontSize: 24),
          ),
        ),
        initialRoute: LoginScreenRoute,
        onGenerateRoute: (RouteSettings routeSettings) {
          Map<String, dynamic> args = routeSettings.arguments;
          switch (routeSettings.name) {
            case LoginScreenRoute:
              return MaterialPageRoute(
                builder: (context) => LoginPage(),
              );
            case HomeScreenRoute:
              return MaterialPageRoute(builder: (context) {
                StudentsService ss = StudentsService();
                ss.getStudents();
                return MultiProvider(
                  providers: [
                    ChangeNotifierProvider<StudentsService>(
                      create: (context) {
                        return ss;
                      },
                    ),
                    ChangeNotifierProvider<AddStudentService>(
                      create: (_) => AddStudentService(),
                    )
                  ],
                  child: Homepage(),
                );

                // ChangeNotifierProvider<StudentsService>(
                //   create: (context) {
                //     return ss;
                //   },
                //   child: Homepage(),
                // );
              });
            case StudentDetailsSceenRoute:
              return MaterialPageRoute(builder: (context) {
                StudentService ss = StudentService();
                ss.getStudent(args['studentId']);
                return ChangeNotifierProvider<StudentService>(
                  create: (context) {
                    return ss;
                  },
                  child: StudentDetailsPage(),
                );
              });
            case ProfilePageRoute:
              return MaterialPageRoute(
                builder: (context) => ProfilePage(),
              );
            default:
              return null;
          }
        },
      ),
    );
  }
}
